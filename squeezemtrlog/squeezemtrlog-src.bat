@echo off
powershell -command "get-content squeezemtrlog.bat | ?{$s -or ($s = $_ -match '^#scriptstart$')} | set-content -path ('{0}\squeezemtrlog.ps1' -f $env:temp)"
powershell -executionpolicy remotesigned -file "%temp%\squeezemtrlog.ps1" -p "%cd%"
del "%temp%\squeezemtrlog.ps1"
exit /b
rem --- End of batch script ---
