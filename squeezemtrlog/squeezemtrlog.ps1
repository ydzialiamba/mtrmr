#scriptstart
param([string]$p)
[environment]::currentdirectory = $p
$logDir = "mtrlogs"
$outDir = "squeezedlogs"

function Run()
{
    if( !(Test-Path $logDir) )
    {
        ExitOnError "Error: Cannot find $logDir directory"
    }
    
    if( !(test-path $outDir) )
    {
        "Creating log folder: $outDir"
        mkdir $outDir | out-null
    }

    $files = dir -file "$logDir\*.txt"
    
    foreach( $file in $files )
    {
        $outFile = "{0}\{1}" -f $outDir, $file.Name
        GetReport ($file.FullName) $outFile
    }
    
    Write-Host "Processing complete"
    Write-Host "Press Enter to exit"
    Read-Host | out-null
    exit 0
}

function ExitOnError( $msg )
{
    Write-Host $msg
    Write-Host "Press Enter to exit"
    Read-Host | out-null
    exit 1
}

function GetReport( $fName, $outName )
{
    Write-Host "Processing $fName"
    $parsed = ParseMtrLog $fName
    GetErrorsTable $parsed $outName $false
}

function ParseMtrLog( $fName )
{
    $content = type $fName
    $result = (new-object collections.generic.list[object] )

    $lineNum = 0

    foreach( $line in $content )
    {
        $lineNum++

        if( $line -match "^-{4}\s+-{6}" )
        {
            $capture = $true;
            $arr = new-object collections.generic.list[object];
            continue;
        }
        
        if( $line -match "^\W*$" )
        {
            if( $capture )
            {
                $result.add( $arr )
                $capture = $false;
            }
            continue;
        }
        
        if( $line -match "\d{4}(?<datedelim>\D)\d{2}\D\d{2} \d{2}(?<timedelim>\D)\d{2}\D\d{2}" )
        {
            $datedelim = $matches["datedelim"]
            $timedelim = $matches["timedelim"]
            $t = [datetime]::ParseExact( $line,('yyyy\{0}MM\{0}dd HH\{1}mm\{1}ss' -f $datedelim,$timedelim), [Globalization.CultureInfo]::InvariantCulture )
        }

        if( $capture )
        {
            if( $line -notmatch "(?<host>\S+)\s+(?<errors>\S+)\s+(?<rttmin>\S+)\s+(?<rttmax>\S+)\s+(?<rttavg>\S+)" )
            {
                $result.add( $arr )
                $capture = $false;
                continue;
            }

            $e = $matches["errors"]

            if( $e.contains( "/" ) )
            {
                $e = $e.Substring(0,$e.IndexOf("/"))
            }
        
            $arr.add( [pscustomobject] @{
                time = $t
                line = $lineNum
                host = $matches["host"]
                errors = $e
                rttMin = $matches["rttmin"]
                rttMax = $matches["rttmax"]
                rttAvg = $matches["rttavg"]
            } )
        }
    }

    return $result
}

function gethop($log,$i){ $result=@();foreach($trace in $log){ $result+=@($trace[$i]) }; return $result }

function GroupByHop( $parsedLog )
{
    $hops = @{}

    foreach( $trace in $parsedLog )
    {
        for( $i = 0; $i -lt $trace.count; $i++ )
        {
            $hops[$i]+=@($trace[$i])
        }
    }
    
    return $hops
}

function GroupStats( $fName )
{
    $r = ParseLog $fName
    $stats = GroupByHop $r
    $stats.Keys | %{ "HOP #$($_+1)"; stats[$_] | group Errors }
}

function GetErrorsTable( $parsedLog, $outFile, $skipZeros = $true )
{
    foreach( $trace in $parsedLog )
    {
        if( $skipZeros )
        {
            $found = $false
            
            foreach( $hop in $trace )
            {
                if( $hop.errors -ne "0" )
                {
                    $found = $true
                }
            }
            
            if( !$found )
            {
                continue
            }
        }
    
        $sb = new-object text.stringbuilder
    
        foreach( $hop in $trace )
        {
            $sb.Append( " " ) | out-null
            $e = $hop.errors
            if( $e -eq "0" )
            {
                $sb.Append( "  " ) | out-null
            }
            else
            {
                $sb.Append( $e.PadLeft(2, " ") ) | out-null
            }
        }
            
        $line = $sb.ToString()  #($trace | %{ $e=$_.errors;if($e -eq "0" -or $e -eq "-"){ "  " }else{ "{0:00}" -f $e } } ) -join " "
        $line = "{0:yyyy/MM/dd HH:mm:ss} {1}|" -f $trace[0].time, $line
        $line | out-file $outFile -enc ascii -append
    }
}

Run