#script_start
$LogFileName = "migratesmconfig.log"

$SM4_PATH = "C:\Program Files (x86)\StreamTheWorld";
$SM5_PATH = "C:\Program Files (x86)\Triton Digital\Station Manager";

$DESTINATION_FOLDER = "Server\Data\Config\Local\";
$MEDIA_ENCODER = "StationManager Server\AddInsData\mediaencoder\MediaEncoderConfig.xml";
$MEDIA_PROXY = "StationManager Server\AddInsData\mediaproxy\config.xml";
$RAS_ADAPTER = "StationManager Server\AddInsData\rasadapter\RasAdapter.xml";
$STATION_MANAGER = "StationManager Server\StationManagerLocal.xml";

function Log($message)
{
    Write-Host $message
    $LogFilePath = [IO.Path]::Combine($env:temp,$LogFileName)
    $logMessage = "[{0:yyyy/MM/dd HH:mm:ss}] {1}" -f [DateTime]::Now,$message
    Out-File -FilePath $LogFilePath -InputObject $logMessage -Append
}

function CopyConfig($srcFileName, $dstFileName, $proj, $skipped)
{
    $srcPath = [IO.Path]::Combine($SM4_PATH, $srcFileName)
    
    if( !(Test-Path $srcPath) )
    {
        Log "File doesn't exist, skipping $srcPath"
        $skipped.Add("'$proj'")
        return
    }
    
    $dstFolder = [IO.Path]::Combine($SM5_PATH, $DESTINATION_FOLDER)
    
    if( !(Test-Path $dstFolder) )
    {
        mkdir $dstFolder | Out-Null
    }
    
    $dstPath = [IO.Path]::Combine($dstFolder, $dstFileName)
    
    Log "Copying '$srcPath' to '$dstPath'"
    copy $srcPath $dstPath
}

if( Test-Path $SM5_PATH )
{
    Log "It seems you already have Station Manager 5 on this system."
    $answer = Read-Host "Would you like to override Station Manager 5 configuration? (y/n)"
    if( $answer -ne "y" )
    {
        Log "Canceled by user"
        Exit 1
    }
}

try
{
    $skipped = (new-object collections.generic.list[string])
    CopyConfig $MEDIA_PROXY "MediaProxy.xml" "Media Proxy" $skipped
    CopyConfig $RAS_ADAPTER "RasAdapter.xml" "Ras Adapter" $skipped
    CopyConfig $STATION_MANAGER "StationManager.xml" "Station Manager" $skipped
    CopyConfig $MEDIA_ENCODER "MediaEncoderConfig.xml" "Media Encoder" $skipped
}
catch
{
    Log "An error occured"
    Log $_
    Read-Host
    Exit 1
}

if( $skipped.count -eq 0 )
{
    Log "***********"
    Log "* Success *"
    Log "***********"
    Read-Host
    Exit 0
}
elseif( $skipped.count -eq 4 )
{
    Log "**************************************************"
    Log "* Error! The script failed to migrate any files. *"
    Log "**************************************************"
    Read-Host
    Exit 1
}
else
{
    [Func[string,string,string]] $delegate = { param($resultSoFar, $next); "$resultSoFar, $next" }
    $missedComponents = [Linq.Enumerable]::Aggregate($skipped, $delegate)
    $msg = "* Warning! The script wasn't able to migrate configuration for: $missedComponents *"
    $lineMsg = "*" * $msg.Length
    
    Log $lineMsg
    Log $msg
    Log $lineMsg
    Read-Host
    Exit 0
}