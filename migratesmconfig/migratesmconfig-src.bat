@echo off
@setlocal enableextensions
rem change current directory to the ~d ( drive letter, 'c:' for example ) plus ~p ( path '\script\' if ran from c:\script\migratesmconfig.bat ) of 0 param, which is current file
@cd /d "%~dp0"
powershell -command "get-content migratesmconfig.bat | ?{$s -or ($s = $_ -match '^#script_launcher_start$')} | set-content -path $env:temp\scriptlauncher.ps1"
powershell -executionpolicy remotesigned -file "%temp%\scriptlauncher.ps1"
del "%temp%\scriptlauncher.ps1"
exit /b
rem --- End of batch script ---
#script_launcher_start
$currentPs = $psversiontable.psversion
if($currentPs -lt [Version]"3.0")
{
    Write-Host "WARNING: Only Powershell 3.0 and higher supported. Current version: $currentPs"
    Read-Host
    Exit 1
}
get-content $PSCommandPath | ?{$s -or ($s = $_ -match '^#script_start$')} | set-content -path $env:temp\script.ps1
$ps = start-process powershell -argumentList "-executionpolicy","remotesigned","-file",('"{0}\script.ps1"' -f $env:temp),"-p",('"{0}"' -f $p) -verb runas -passthru
$ps.WaitForExit()
del "$($env:temp)\script.ps1"
Exit 0
#script_launcher_end : keep the new line ->
