@echo off
@setlocal enableextensions
@set workdir=%cd%
@cd /d "%~dp0"
powershell -command "get-content %~n0.bat | ?{$s -or ($s = $_ -match '^#script.ps1$')} | set-content -path ('{0}\script.ps1' -f $env:temp)"
powershell -executionpolicy remotesigned -file "%temp%\script.ps1" -scriptpath "%cd%" -workdir "%workdir%"
del "%temp%\script.ps1"
exit /b
rem --- End of batch script ---
