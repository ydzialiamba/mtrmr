@echo off
del "%~1.zip"
if "%~2" == "elevated" (
echo copy /b bootstrapper_elevated.bat+script.ps1 "%~1.bat" > build.bat
7z a "%~1.zip" bootstrapper_elevated.bat script.ps1 build.bat
) else (
echo copy /b bootstrapper.bat+script.ps1 "%~1.bat" > build.bat
7z a "%~1.zip" bootstrapper.bat script.ps1 build.bat
)
del build.bat