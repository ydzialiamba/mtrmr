@echo off
@setlocal enableextensions
@set workdir=%cd%
@cd /d "%~dp0"
powershell -command "get-content %~n0.bat | ?{$s -or ($s = $_ -match '^#scriptlauncher.ps1$')} | ?{-not ($e -or ($e = $_ -match '^#script.ps1$'))} | set-content -path ('{0}\scriptlauncher.ps1' -f $env:temp)"
powershell -command "get-content %~n0.bat | ?{$s -or ($s = $_ -match '^#script.ps1$')} | set-content -path ('{0}\script.ps1' -f $env:temp)"
powershell -executionpolicy remotesigned -file "%temp%\scriptlauncher.ps1" -scriptpath "%cd%" -workdir "%workdir%"
del "%temp%\scriptlauncher.ps1"
del "%temp%\script.ps1"
exit /b
#scriptlauncher.ps1
param([string]$scriptpath,[string]$workdir)
$ps = start-process powershell -argumentList "-executionpolicy","remotesigned","-file",('"{0}\script.ps1"' -f $env:temp),"-scriptpath",('"{0}"' -f $scriptpath),"-workdir",('"{0}"' -f $workdir) -verb runas -passthru
$ps.WaitForExit()
#script.ps1
param([string]$scriptpath,[string]$workdir)

# Write your code here