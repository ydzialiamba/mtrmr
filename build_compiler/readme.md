# Compiler of bat file builder

This project creates a zip file that contains a bat file builder build.bat as well as auxilary bootstrapper and script files.

- Run compile.bat with a name of the project and elevated keyword for a script that requires admin rights.

* Elevated *

```powershell
compile.bat useful_tool elevated
```
* Not elevated *

```powershell
compile.bat useful_tool
```


- Copy the resulting zip ( in this example it's useful_tool.zip ) to another folder.

- Once you added code into script.ps1, run build.bat and it'll create the final bat file. In the example it'll be useful_tool.bat. 