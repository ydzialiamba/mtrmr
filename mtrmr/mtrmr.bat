@echo off
powershell -command "get-content mtrmr.bat | ?{$s -or ($s = $_ -match '^#mtrmr.ps1$')} | set-content -path ('{0}\mtrmr.ps1' -f $env:temp)"
powershell -executionpolicy remotesigned -file "%temp%\mtrmr.ps1" -p "%cd%"
del "%temp%\mtrmr.ps1"
exit /b
rem --- End of batch script ---
#mtrmr.ps1
param([string]$p)
[environment]::currentdirectory = $p

$Global:ProgressPreference = 'SilentlyContinue'
$logDir = "mtrlogs"

function VerifyPath( $path )
{
    if( !(Test-Path $path) )
    {
        Write-Host "Error: Cannot find $path"
        Write-Host "Press any key to exit"
        Read-Host | out-null
        exit 1
    }
}

function TraceRoute( $ip )
{
    if(get-command test-netconnection -ea silentlycontinue)
    {
        $testInfo = Test-NetConnection $ip -TraceRoute -WarningAction SilentlyContinue
        return $testInfo | select -expand TraceRoute
    }
    else
    {
        $r = (tracert -d -4 $ip)
        return ($r | sls "^\s*(?<num>\d+).*\s(?<ip>(\d{1,3}\.){3}\d{1,3})|(?<timeout>Request timed out\.)\s*$" | %{ $ip=$_.matches[0].groups["ip"].value; if($ip){$ip}else{"0.0.0.0"} })
    }
}

function Ping( $ip, $count = 20, $bufLen = 32 )
{
    if( $ip -eq "0.0.0.0" )
    {
        return [PSCustomObject]@{
            Host = "TimedOut"
            Errors = "-"
            RTTMin = "-"
            RTTMax = "-"
            RTTAvg = "-"
        }
    }
    
    $ping = new-object Net.NetworkInformation.Ping
    $rnd = new-object Random
    $buf = new-object byte[] $bufLen
    $rnd.NextBytes( $buf )
    
    $errors = 0
    
    $RTTs = new-object Collections.Generic.List[int]
    
    1..$count | %{
        $pingFail = $false
    
        try
        {
            $pingResult = $ping.Send( $ip, 1000, $buf ) #Send the packet with a 1 second timeout
        }
        catch
        {
            $pingFail = $true
        }
        
        if( $pingFail -or $pingResult.Status -ne [net.networkinformation.ipstatus]::success )
        {
            $errors++
        }
        else
        {
            $RTT = $pingResult.RoundtripTime
            $RTTs.Add( $RTT )
        }
    }
    
    $stats = $RTTs | measure -minimum -maximum -average
    
    $errorsVal = "{0}/{1}" -f $errors,$count
    
    if( $RTTs.Count -gt 0 )
    {
        $avgVal = "{0:N1}" -f $stats.Average
        $minVal = $stats.Minimum
        $maxVal = $stats.Maximum
    }
    else
    {
        $avgVal = "N/A"
        $minVal = "N/A"
        $maxVal = "N/A"
    }
    
    return [PSCustomObject]@{
        Host = $ip
        Errors = $errorsVal
        RTTMin = $minVal
        RTTMax = $maxVal
        RTTAvg = $avgVal
    }
}

function Mtr( $ip )
{
    $result = @()

    foreach( $hop in TraceRoute( $ip ) )
    {
        $result += Ping( $hop )
    }
    
    $result | Format-Table -Autosize
}

function RunMtrForMrs( $mrs )
{
    $prevLogFiles = @{}

    if( !(test-path $logDir) )
    {
        "Creating log folder: $logDir"
        mkdir $logDir | out-null
    }

    while( $true )
    {
        while(($last -ne $null) -and (([datetime]::now - $last) -lt [timespan]::fromminutes(1)) )
        {
            [threading.thread]::sleep( 1000 )
        }
        
        $last = [datetime]::now
            
        foreach( $mr in $mrs )
        {
            $curTime = [datetime]::now
            $logFile = "{0:yyyy_MM_dd}__$($mr.id).txt" -f $curTime
            $logPath = [io.path]::combine( $logDir, $logFile )
            
            if( $logFile -ne $prevLogFiles[$mr.id] )
            {
                $prevLogFiles[$mr.id] = $logFile
                "Writing MTR log to $logFile"
                "=== MR ID:$($mr.id) MR HOST:$($mr.host) ===" | out-file $logPath -enc ascii
            }

            $mtrResult = Mtr $mr.host
            
            "{0:yyyy/MM/dd HH:mm:ss}" -f $curTime | out-file $logPath -enc ascii -append
            $mtrResult | out-file $logPath -enc ascii -append
        }
    }
}

function GetMrsFromConfig( $config )
{
    $configVersion = $config["v"]
    $configPath = $config["path"]
    
    if( $configVersion -eq 5 )
    {
        $idAttr = "Id"
        $hostAttr = "Host"
    }
    else
    {
        $idAttr = "id"
        $hostAttr = "host"
    }
    
    $xmlConfig = [xml](type $configPath)
    $mrIds = $xmlConfig.SelectNodes("//MediaRelay").GetAttribute($idAttr) | select -unique
    $mrs = $mrIds | %{ $mrId=$_;$mrHost=$xmlConfig.SelectSingleNode("//MediaRelay[@$idAttr='$mrId']").GetAttribute($hostAttr); [pscustomobject]@{ id=$mrId; host=$mrHost } }
    return $mrs
}

function GetConfig()
{
    $config = GetSm5Config
    if( $config )
    {
        Write-Host "Found SM5 configuration"
        return @{ v=5; path=$config}
    }
    
    $config = GetSm4Config
    if( $config )
    {
        Write-Host "Found SM4 configuration"
        return @{ v=4; path=$config}
    }

    $config = GetZipStreamConfig
    if( $config )
    {
        Write-Host "Found ZipStream configuration"
        return @{ v=4; path=$config}
    }
    
    ExitOnError "Error: Cannot find Station Manager configuration"
}

function ExitOnError( $msg )
{
    Write-Host $msg
    Write-Host "Press any key to exit"
    Read-Host | out-null
    exit 1
}

function GetSm5Config()
{
    $configPath = "c:\Program Files (x86)\Triton Digital\Station Manager\Server\Data\Config\Provisioning\StationManager.xml"
    if( Test-Path $configPath )
    {
        return $configPath
    }
    
    return $null
}

function GetSm4Config()
{
    $smPath = "c:\Program Files (x86)\StreamTheWorld\StationManager Server\"
    
    if( !(Test-Path $smPath) )
    {
        return $null
    }
    
    $provConfig = "StationManagerProv.xml"
    $provConfigPath = [io.path]::combine( $smPath, $provConfig )

    if( !(Test-Path $provConfigPath) )
    {
        Write-Host "Cannot find $provConfigPath"
        return $null
    }

    $provConfig = [xml](type $provConfigPath)

    $mpVersion = $provConfig.SelectSingleNode("//component[@type='mediaproxy']").GetAttribute("version")

    Write-Host "Detected MP version: $mpVersion"

    $mpConfigPath = [io.path]::combine( $smPath, "AddIns\mediaproxy-$mpVersion\MediaProxy.xml" )

    if( Test-Path $mpConfigPath )
    {
        return $mpConfigPath
    }
    
    Write-Host "Cannot find $mpConfigPath"
    return $null
}

function GetZipStreamConfig()
{
    if( !(Test-Path env:activebank) )
    {
        return $null
    }
    
    Write-Host "Detected ZipStream"
    
    $activeBank = (Get-Item env:activebank).value
    $smPath = "D:\Programs\banks\$activeBank\StationManager Server\"
    
    if( !(Test-Path $smPath) )
    {
        Write-Host "Cannot find $smPath"
        return $null
    }
    
    $provConfig = "StationManagerProv.xml"
    $provConfigPath = [io.path]::combine( $smPath, $provConfig )

    if( !(Test-Path $provConfigPath) )
    {
        Write-Host "Cannot find $provConfigPath"
        return $null
    }

    $provConfig = [xml](type $provConfigPath)

    $mpVersion = $provConfig.SelectSingleNode("//component[@type='mediaproxy']").GetAttribute("version")

    "Detected MP version: $mpVersion"

    $mpConfigPath = [io.path]::combine( $smPath, "AddIns\mediaproxy-$mpVersion\MediaProxy.xml" )

    if( !(Test-Path $mpConfigPath) )
    {
        Write-Host "Cannot find $mpConfigPath"
        return $null
    }
    
    return $mpConfigPath
}

function StartMtr()
{
    $config = GetConfig
    
    $mrs = GetMrsFromConfig $config

    "Detected MRs:"
    $mrs | %{ $_.host }
    
    RunMtrForMrs( $mrs )
}

StartMtr