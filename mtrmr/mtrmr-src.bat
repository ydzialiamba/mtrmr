@echo off
powershell -command "get-content mtrmr.bat | ?{$s -or ($s = $_ -match '^#mtrmr.ps1$')} | set-content -path ('{0}\mtrmr.ps1' -f $env:temp)"
powershell -executionpolicy remotesigned -file "%temp%\mtrmr.ps1" -p "%cd%"
del "%temp%\mtrmr.ps1"
exit /b
rem --- End of batch script ---
