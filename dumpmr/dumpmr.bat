@echo off
powershell -command "get-content dumpmr.bat | ?{$s -or ($s = $_ -match '^#script.ps1$')} | set-content -path ('{0}\script.ps1' -f $env:temp)"
powershell -executionpolicy remotesigned -file "%temp%\script.ps1" -p "%cd%"
del "%temp%\script.ps1"
exit /b
rem --- End of batch script ---
#script.ps1
param([string]$p)
[environment]::currentdirectory = $p

function VerifyPath( $path )
{
    if( !(Test-Path $path) )
    {
        Write-Host "Error: Cannot find $path"
        Write-Host "Press any key to exit"
        Read-Host | out-null
        exit 1
    }
}

function GetConfig()
{
    $config = GetSm5Config
    if( $config )
    {
        Write-Host "Found SM5 configuration"
        return @{ v=5; path=$config}
    }
    
    $config = GetSm4Config
    if( $config )
    {
        Write-Host "Found SM4 configuration"
        return @{ v=4; path=$config}
    }

    $config = GetZipStreamConfig
    if( $config )
    {
        Write-Host "Found ZipStream configuration"
        return @{ v=4; path=$config}
    }
    
    ExitOnError "Error: Cannot find Station Manager configuration"
}

function GetSm5Config()
{
    $configPath = "c:\Program Files (x86)\Triton Digital\Station Manager\Server\Data\Config\Provisioning\StationManager.xml"
    if( Test-Path $configPath )
    {
        return $configPath
    }
    
    return $null
}

function GetSm4Config()
{
    $smPath = "c:\Program Files (x86)\StreamTheWorld\StationManager Server\"
    
    if( !(Test-Path $smPath) )
    {
        return $null
    }
    
    $provConfig = "StationManagerProv.xml"
    $provConfigPath = [io.path]::combine( $smPath, $provConfig )

    if( !(Test-Path $provConfigPath) )
    {
        Write-Host "Cannot find $provConfigPath"
        return $null
    }

    $provConfig = [xml](type $provConfigPath)

    $mpVersion = $provConfig.SelectSingleNode("//component[@type='mediaproxy']").GetAttribute("version")

    Write-Host "Detected MP version: $mpVersion"

    $mpConfigPath = [io.path]::combine( $smPath, "AddIns\mediaproxy-$mpVersion\MediaProxy.xml" )

    if( Test-Path $mpConfigPath )
    {
        return $mpConfigPath
    }
    
    Write-Host "Cannot find $mpConfigPath"
    return $null
}

function GetZipStreamConfig()
{
    if( !(Test-Path env:activebank) )
    {
        return $null
    }
    
    Write-Host "Detected ZipStream"
    
    $activeBank = (Get-Item env:activebank).value
    $smPath = "D:\Programs\banks\$activeBank\StationManager Server\"
    
    if( !(Test-Path $smPath) )
    {
        Write-Host "Cannot find $smPath"
        return $null
    }
    
    $provConfig = "StationManagerProv.xml"
    $provConfigPath = [io.path]::combine( $smPath, $provConfig )

    if( !(Test-Path $provConfigPath) )
    {
        Write-Host "Cannot find $provConfigPath"
        return $null
    }

    $provConfig = [xml](type $provConfigPath)

    $mpVersion = $provConfig.SelectSingleNode("//component[@type='mediaproxy']").GetAttribute("version")

    "Detected MP version: $mpVersion"

    $mpConfigPath = [io.path]::combine( $smPath, "AddIns\mediaproxy-$mpVersion\MediaProxy.xml" )

    if( !(Test-Path $mpConfigPath) )
    {
        Write-Host "Cannot find $mpConfigPath"
        return $null
    }
    
    return $mpConfigPath
}

function GetMrsFromConfig( $config )
{
    $configVersion = $config["v"]
    $configPath = $config["path"]
    
    if( $configVersion -eq 5 )
    {
        $idAttr = "Id"
        $hostAttr = "Host"
    }
    else
    {
        $idAttr = "id"
        $hostAttr = "host"
    }
    
    $xmlConfig = [xml](type $configPath)
    $mrIds = $xmlConfig.SelectNodes("//MediaRelay").GetAttribute($idAttr) | select -unique
    $mrs = $mrIds | %{ $mrId=$_;$mrHost=$xmlConfig.SelectSingleNode("//MediaRelay[@$idAttr='$mrId']").GetAttribute($hostAttr); [pscustomobject]@{ id=$mrId; host=$mrHost } }
    return $mrs
}

function StartDump()
{
    $config = GetConfig
    $mrs = GetMrsFromConfig $config

    "Detected MRs:"
    $mrs | %{ $_.host }

    $wiresharkPath = "c:\Program Files\Wireshark\" 
    $dumpCapExe = "dumpcap.exe"
    $dumpCap = [io.path]::combine( $wiresharkPath, $dumpCapExe )

    VerifyPath $dumpCap

    "Available network interfaces:"

    . $dumpCap -D

    $iNum = [int](Read-Host "Please select a network interface for the capture")

    $mrs | %{ if( !(test-path $_.id) ){ "Creating folder $($_.id)"; mkdir $_.id | out-null } }
    $mrs | %{ start-process -FilePath $dumpCap -ArgumentList "-i","$iNum","-w","$($_.id)\mr$($_.id).pcap","-f","`"host $($_.host)`"","-b","interval:3600" }

    Write-Host "Press any key to exit"
    Read-Host | out-null
}

StartDump