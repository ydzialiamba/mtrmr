@echo off
powershell -command "get-content dumpmr.bat | ?{$s -or ($s = $_ -match '^#script.ps1$')} | set-content -path ('{0}\script.ps1' -f $env:temp)"
powershell -executionpolicy remotesigned -file "%temp%\script.ps1" -p "%cd%"
del "%temp%\script.ps1"
exit /b
rem --- End of batch script ---
